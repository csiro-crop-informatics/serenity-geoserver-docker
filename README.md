docker-geoserver
================

Dockerized GeoServer.

Customisation of [oscarfonts/geoserver](https://github.com/oscarfonts/docker-geoserver).  
Addition of script to add our existing postgres database as a datastore through API calls
after initialisation, using environment variable. 

## Features

* Built on top of [Docker's official tomcat image](https://hub.docker.com/_/tomcat/).
* Taken care of [JVM Options](http://docs.geoserver.org/latest/en/user/production/container.html), to avoid PermGen space issues &c.
* Separate GEOSERVER_DATA_DIR location (on /var/local/geoserver).
* [CORS ready](http://enable-cors.org/server_tomcat.html).
* Up to GeoServer 2.14.x: Automatic installation of [Native JAI and Image IO](http://docs.geoserver.org/latest/en/user/production/java.html#install-native-jai-and-jai-image-i-o-extensions) for better performance.
* From GeoServer 2.15.x: OpenJDK 11.
* Configurable extensions.
* Automatic installation of [Microsoft Core Fonts](http://www.microsoft.com/typography/fonts/web.aspx) for better labelling compatibility.
* AWS configuration files and scripts in order to deploy easily using [Elastic Beanstalk](https://aws.amazon.com/documentation/elastic-beanstalk/). See [github repo](https://github.com/oscarfonts/docker-geoserver/blob/master/aws/README.md). Thanks to @victorzinho

## Running

Get the image:

```
docker pull spriggsy83/geoserver
```

Run as a service, exposing port 8080 and using a hosted GEOSERVER_DATA_DIR:

```
docker run -d -p 8600:8080 -v /path/to/local/data_dir:/var/local/geoserver \
  -e DATABASE=postgres://[user]:[password]@[host]:[port]/
  --name=MyGeoServerInstance spriggsy83/geoserver
```

(On a local machine setup, with Postgres in seprate Docker container, 
host may be 'host.docker.internal' rather than 'localhost' or similar)

### Configure path

It is possible to configure the context path by providing a Catalina configuration directory:

```
docker run -d -p 8600:8080 \
  -v /path/to/local/conf_dir:/usr/local/tomcat/conf/Catalina/localhost oscarfonts/geoserver
```

See some [examples](https://github.com/oscarfonts/docker-geoserver/tree/master/2.9.1/conf).

### Logs

See the tomcat logs while running:

```
docker logs -f MyGeoServerInstance
```

## Fresh build
```
cd 2.15.1
docker build -t [name/tag] .
```
