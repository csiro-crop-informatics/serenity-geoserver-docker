#!/bin/bash

GEOSERVER_ADMIN_USER=${GEOSERVER_ADMIN_USER:-admin}
GEOSERVER_ADMIN_PASSWORD=${GEOSERVER_ADMIN_PASSWORD:-geoserver}

echo "######## Adding datastore in 2m ########"
date

sleep 2m

TESTRES=$( curl -sL -w '%{http_code}' -XGET \
  -u ${GEOSERVER_ADMIN_USER}:${GEOSERVER_ADMIN_PASSWORD} \
  http://localhost:8080/geoserver/rest/workspaces/serenity \
  -o /dev/null );
if [ $TESTRES = "200" ] 
then
  echo "######## Serenity workspace already exists ########"
else
  echo "######## Adding Serenity workspace ########"
  curl -v -u ${GEOSERVER_ADMIN_USER}:${GEOSERVER_ADMIN_PASSWORD} \
    -XPOST -H "Content-type: text/xml" \
    -d "<workspace><name>serenity</name></workspace>" \
    http://localhost:8080/geoserver/rest/workspaces
fi

TESTRES=$( curl -sL -w '%{http_code}' -XGET \
  -u ${GEOSERVER_ADMIN_USER}:${GEOSERVER_ADMIN_PASSWORD} \
  http://localhost:8080/geoserver/rest/workspaces/serenity/datastores/serenity \
  -o /dev/null );
if [ $TESTRES = "200" ] 
then
  echo "######## Serenity datastore already exists ########"
else
  echo "######## Adding Serenity datastore ########"

  ## Convert database connection string to xml components
  DBXML=$( echo $DATABASE | \
    perl -pe \
    's/.+?\/\/(\w+)\:(.+?)\@(.+?)\:(\d+)\/(\w+)(\?.*)?$/<host>$3<\/host><port>$4<\/port><database>$5<\/database><user>$1<\/user><passwd>$2<\/passwd>/' )
  
  curl -v -u ${GEOSERVER_ADMIN_USER}:${GEOSERVER_ADMIN_PASSWORD} \
    -XPOST -H "Content-type: text/xml" \
    -d "<dataStore><name>serenity</name><connectionParameters>${DBXML}<dbtype>postgis</dbtype></connectionParameters></dataStore>" \
     http://localhost:8080/geoserver/rest/workspaces/serenity/datastores
fi

# postgres://serenity:serenity@host.docker.internal:5432/postgres
